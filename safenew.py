import sys

def cell(r, c, fs, bs):
    box = [[0] * c for i in range(r)]
    for i, j, _ in fs:
        if i <= r and j <= c:      #if u give 5 rows and slash is palced at 6th row mean then that is not possible.
            box[i-1][j-1] = '/'
    for i, j, _ in bs:
        if i <= r and j <= c:
            box[i-1][j-1] = '\\'
    return box

def empty_cells(r, c, box):
    count = 0
    for i in range(r):
        for j in range(c):
            if box[i][j] == 0:
                count += 1
    if count == r * c:
        return True
    else:
        return False


def missing_mirror_count(r,c,box) :
    count = 0
    pos = ()
    for i in range(r-1) :
        for j in range(c-1) :
            if box[i][j] != 0 and (box[i+1][j+1] != 0 or box[i+1][j] != 0 or box[i][j+1] != 0) :
                count += 1
                pos = (i+1,j+1)
    print(count,pos)

def solve(r, c, m, n, slash, backslash):
    state = cell(r, c, slash, backslash)

    if empt_cells(r, c, state):
        return "impossible"

    count, pos = missing_mir_count(r, c, state)
    if count == 0:
        return "0"
    elif count == 1:
        return f"1 {pos[0]} {pos[1]}"
    else:
        return f"{count} {pos[0]} {pos[1]}"


inp = sys.stdin.readlines()
r,c,m,n = map(int,inp[0].split())
slash = [tuple(map(int, inp[i].split())) + (0,) for i in range(1,m + 1)]
backslash = [tuple(map(int,inp[i].split())) + (0,) for i in range(m+1,n)]
 
result = solve(r, c, m, n, slash, backslash)
print(result)


	
